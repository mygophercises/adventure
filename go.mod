module gitlab.com/mygophercises/adventure

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1
	github.com/stretchr/testify v1.7.0
)
