package domain

import (
	"encoding/json"
	"strings"

	"github.com/mitchellh/mapstructure"
)

type Story struct {
	Arcs map[string]Arc
}

type Arc struct {
	Story   []string
	Title   string
	Options []Option
}

type Option struct {
	Arc  string
	Text string
}

func NewStory(jsonData string) (*Story, error) {
	var jsonMap map[string]interface{}
	err := json.NewDecoder(strings.NewReader(jsonData)).Decode(&jsonMap)

	if err != nil {
		return nil, err
	}
	arcs := map[string]Arc{}
	for k, v := range jsonMap {
		var arc Arc
		err := mapstructure.Decode(v, &arc)
		if err != nil {
			return nil, err
		}

		arcs[k] = arc
	}
	return &Story{Arcs: arcs}, nil
}
