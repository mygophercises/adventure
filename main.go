package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/mygophercises/adventure/domain"
)

const hostUrl string = "http://localhost:8080"

var (
	story *domain.Story
)

func arcHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	arc := story.Arcs[vars["arc"]]
	var htmlTemplate string
	if vars["arc"] == "home" {
		htmlTemplate = "templates/home.html"
	} else {
		htmlTemplate = "templates/arc.html"
	}
	parsedTemplate, err := template.ParseFiles(htmlTemplate)
	if err != nil {
		log.Fatal(err)
		return
	}
	err = parsedTemplate.Execute(w, arc)
	if err != nil {
		log.Fatal(err)
		return
	}

}
func main() {
	storyData, err := os.Open("story.json")
	if err != nil {
		log.Fatal(err)
	}
	defer storyData.Close()
	storyByteData, err := ioutil.ReadAll(storyData)
	if err != nil {
		log.Fatal(err)
	}

	story, err = domain.NewStory(string(storyByteData))
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, hostUrl+"/intro", http.StatusFound)
	})
	r.HandleFunc("/{arc}", arcHandler)
	http.ListenAndServe(":8080", r)

}
